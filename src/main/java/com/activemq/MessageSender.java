package com.activemq;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;

import org.apache.activemq.ActiveMQConnectionFactory;
import javax.jms.Connection;
import javax.jms.Session;
import javax.jms.TextMessage;

public class MessageSender {


	private static String text = "Hello ActiveMQ!";

	public static boolean publish() throws JMSException {

		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(Constant.URL);
		factory.setUserName(Constant.USERNAME);
		factory.setPassword(Constant.PASSWORD);
		Connection connection = factory.createConnection();
		try {
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createTopic(Constant.TOPIC);

			MessageProducer producer = session.createProducer(destination);
			TextMessage message = session.createTextMessage(text);
			producer.send(message);
			
			System.out.println(">>>Published Message" + message.getText());
			session.close();
		} catch (JMSException e) {
			System.out.println("Error in Message Sender"+e.getMessage());
			throw e;
		} finally {
			connection.close();
		}
		return true;
	}

	
}
