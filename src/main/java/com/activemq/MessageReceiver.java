package com.activemq;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class MessageReceiver {



	public static void consume() throws JMSException {

		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(Constant.URL);
		factory.setUserName(Constant.USERNAME);
		factory.setPassword(Constant.PASSWORD);
		Connection connection = factory.createConnection();

		try {
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createQueue(Constant.QUEUE);

			MessageConsumer consumer = session.createConsumer(destination);
			Message message = consumer.receive();

			if (message instanceof TextMessage) {
				TextMessage textMessage = (TextMessage) message;
				System.out.println("Received message '" + textMessage.getText() + "'");
			}
			
			session.close();
		} catch (JMSException e) {
			System.out.println("Error in MessageSender" + e.getMessage());
			throw e;
		} finally {
			connection.close();
		}
	}


	

}
