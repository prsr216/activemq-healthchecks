package com.activemq;

import javax.jms.JMSException;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "ActiveMQ process started.." );
        try {
			boolean status = MessageSender.publish();
			if(status)
				MessageReceiver.consume();
			
		} catch (JMSException e) {
			e.printStackTrace();
		}
    }
}
